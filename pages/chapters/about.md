---
title: A propos de ce guide et de Koweb
has_children: true
nav_order: 4
uuid: sSN4byuw8ClDepKO-qMry
---
# A propos de ce guide et de Koweb

## Auteurs

Ce guide en ligne a été réalisé par [Laurent Chedanne](https://www.koweb.fr/koweb/equipe/laurent-chedanne). Il a été initialement créé avec le soutien de [Paul Heddi](https://www.koweb.fr/koweb/equipe/paul-heddi) dans [une version PDF avec une jolie mise en page](https://www.koweb.fr/kit-outils-libres).

La coordination des contributeurs-trices à cette ressource est assurée par Laurent Chedanne. **Pour modifier ce guide**, veuillez lire la section [Contribuer](../docs/contribute.html). Rédiger dans ce guide est aussi simple qu'écrire sur un traitement de texte.

## Koweb

Koweb est une marque collective autour de laquelle une communauté de facilitateurs-trices s'est regroupée. Iels ont un objectif principal : 

> améliorer le monde en cultivant l'intelligence, la bienveillance, la dynamique collective et soutenues par une facilitation numérique humaniste.

**La facilitation numérique humaniste**, c'est 

1. augmenter le pouvoir d'agir des équipes et des réseaux grâce au potentiel des outils numériques
2. apporter une dimension humaine et de proximité dans l’usage du numérique
3. veiller à ce que chaque outil collaboratif soit bien choisi et bien approprié
4. proposer des systèmes de formation permanente et sur-mesure pour que personne ne soit mis de côté

**Plusieurs ressources sont mis à disposition de la communauté**. Ce sont des [communs](https://lescommuns.org/) ayant chacun leurs propres règles de gestion communautaire (gouvernance) pour en prendre soin collectivement et servir la raison d'être de Koweb.

> En améliorant le travail des équipes et des réseaux grâce au potentiel des outils numériques, Koweb contribue à augmenter le pouvoir des petits groupes pour améliorer le monde.
>
> * <cite>Raison d'être de Koweb</cite>

Quelques exemples de communs :

* [La marque Koweb](https://www.koweb.fr) : elle est utilisée par [une équipe de professionnels](https://www.koweb.fr/koweb/qui-sommes-nous) développant des activités de formation et accompagnement. [Rejoindre l'équipe](https://www.koweb.fr/koweb/qui-sommes-nous/rejoindre-koweb)
* [Le guide de démarrage pour Soutenir le travail d'équipe avec les outils libres](https://kit-outils-libres.koweb.fr), librement accessible et ouvert à la contribution.
* [Le Koweb Kafé](https://kafe.koweb.fr) : un espace d'entraide et répertoriant des ressources utiles pour tout-e facilitateur-trice numérique. Les ressources sont libres ainsi que l'inscription. C'est le lieu où la communauté se retrouve avec le soutien d'une équipe dédiée à la coordination : les baristas.

La marque Koweb a été déposée via la [Legal Service for Commons](https://lsc.encommuns.org/) pour la protéger *juridiquement* de toute appropriation individuelle ou de tout usage ne respectant pas la raison d'être de Koweb.