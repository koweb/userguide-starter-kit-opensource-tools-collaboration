---
title: Comment aller plus loin ?
nav_order: 3
has_children: false
uuid: K7VkSZDpCqpHYJv8iXVP
---

# Comment aller plus loin ?

## Se former et se faire accompagner

### Koweb

[Koweb](https://www.koweb.fr) vous accompagne à votre rythme pour **augmenter la participation et l’intelligence collective** dans vos ateliers, vos formations, vos évènements, votre équipe ou votre organisation.

> Mettons **l'humain au coeur des usages numériques** pour que le numérique soutienne le développement du meilleur des humains.
> <cite>- Koweb</cite>

### PANA : Point d'Accès aux Numériques Associatifs

Réseau d'acteurs (points d'accès) animé et construit par HelloAsso, le mouvement associatif et la Fonda pour accompagner la transition numérique du monde associatif.

[Trouver un PANA](https://pana-asso.org/)

## Les hébergeurs C.H.A.T.O.N.S

Collectif d’Hébergeurs Associatifs, Transparents, Ouverts, Neutres et Solidaires.

Ce guide vous a fait découvrir par exemple les CHATONS IndieHosters, Framasoft, Ethibox, Zaclys, Colibris.

Trouvez celui qui vous correspond le mieux sur [chatons.org](https://chatons.org) car ils sont nombreux et couvrent tout le territoire.

## Discuter, partager et s'entraider

[Le Koweb Kafé](https://kafe.koweb.fr) est un forum animé par Koweb. C'est un béritable espace communautaire pour toutes les personnes accompagnant les collectifs dans l'usage des outils numériques. Oraganisé autour de discussions, vous y trouverez des ressources, des réflexions et des événements pour découvrir des outils et des pratiques de facilitation d’équipe avec les outils numériques.
