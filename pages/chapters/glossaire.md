---
title: Glossaire
has_children: false
nav_order: 1
uuid: QYfjyZx16Dk9tQTVSWIWn
---
# Le glossaire pour comprendre de quoi nous parlons

![Glossaire](../../assets/uploads/glossaire.jpg)

## Logiciel

Un programme qui peut être exécuté (on dit aussi “lancé” ou qu’il “tourne”) sur une machine. Il fonctionne grâce à un code source qui est une œuvre intellectuelle protégée par le droit d'auteur et de la propriété intellectuelle. Les logiciels libres appliquent une licence donnant la liberté aux personnes de consulter ce code source, le modifier, le distribuer et l'exécuter sur la machine de leur choix.

## Serveur

Un gros ordinateur constamment allumé qui fait tourner des logiciels accessibles à une adresse internet par des logiciels installés sur un autre ordinateur (logiciel client). Par exemple, quand vous utilisez framapad pour écrire à plusieurs un texte, vous utilisez le navigateur internet Firefox (client) pour accéder au logiciel Etherpad installé sur le serveur à l’adresse framapad.org

## Instance

L’exemplaire du logiciel qui est lancé sur un serveur mis en place par un hébergeur à une adresse donnée. Par exemple, je me connecte sur l’instance framapad.org (de l’hébergeur Framasoft) du logiciel Etherpad. Il existe une autre instance du logiciel Etherpad à l’adresse pad.colibris-outilslibres.org et hébergé par le mouvement des Colibris.