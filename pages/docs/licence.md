---
title: "Licence"
parent: "A propos de ce guide et de Koweb"
nav_order: 3
---

# La licence de ce guide

![licence creative commons CC-BY-SA](../../assets/uploads/licence.jpg)

Vous pouvez librement copier, diffuser, modifier et distribuer ce document sur d'autres supports à la condition de citer son auteur (Koweb) avec un lien vers ce guide. [En savoir plus sur la licence](https://www.koweb.fr/koweb/mentions-legales/copyleft).