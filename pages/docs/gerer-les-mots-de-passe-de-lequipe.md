---
title: "Gérer les mots de passe de l'équipe"
nav_order: 6
parent: Les besoins essentiels d'une équipe
---

# Gérer les mots de passe de l'équipe

Aujourd’hui, il est devenu difficile de garder la trace de tous nos mots de passe et de se le transmettre de manière sécurisée. Il existe deux solutions qui dominent le marché : Keepass et Bitwarden. Ces deux solutions permettent de bénéficer d'un coffre fort collaboratif. Ce sont des alternatives à [LastPass](https://www.lastpass.com/fr) et [Dashlane](https://www.dashlane.com/fr).

![Gérer les mots de passe en équipe](../../assets/uploads/gerer-les-mots-de-passe-en-equipe.jpg)

## Les outils libres pour gérer les mots de passe en équipe

### Keepass

[Keepass](https://fr.wikipedia.org/wiki/KeePass) est un gestionnaire de mots de passe qui sont stockés dans un coffre fort. Le coffre fort est sous la forme un fichier portant l'extention `kdb` ou `kdbx` et s'ouvre grâce à une clé et/ou mot de passe maitre. Ce fichier *coffre fort* peut ainsi être partagé afin de transmettre des mots de passe à un cercle restreint de personnes. De nombreux outils de gestion du coffre fort existent et en fonction de ceux choisis, la stratégie de partage va varier. Vous pouvez vous appuyer sur de nombreux logiciels proposés dans ["Où démarrer ?"](#o%C3%B9-d%C3%A9marrer-)

### Bitwarden / Vaultwarden

[Bitwarden](https://bitwarden.com/) est un gestionnaire de mots de passe. Le coffre fort est stocké sur [un serveur](../chapters/glossaire.md) et [des logiciels à installer](https://bitwarden.com/download/) sur son ordinateur, mobile ou navigateur facilitent l'usage de ces mots de passe.

[Vaultwarden](https://github.com/dani-garcia/vaultwarden) est ce que l'on appelle un fork de Bitwarden, c'est à dire un nouveau logiciel créé à partir du code source d'un logiciel existant.

### Comment choisir entre les deux ?

La différence principale entre ces deux solutions résident dans **le lieu où sont sockés les mots de passe** qui induit une facilité plus ou moins en grande de mise en oeuvre et d'utilisation du coffre fort. Lisez-bien le [glossaire](../chapters/glossaire.md) pour comprendre !

- Le coffre fort keepass est un fichier (comme si on avait un fichier Word) alors que dans le cas de Bitwarden, le coffre fort est géré par un serveur central. Cette différence d'architecture permet à Bitwarden de proposer [des logiciels à installer](https://bitwarden.com/download/) qui facilite l'accès aux mots de passe peu importe l'endroit où on en a besoin.
- Bitwarden nécessite de créer ou trouver une instance serveur de [Bitwarden server](https://github.com/bitwarden/server).

> Keepass est plus simple à mettre en place au sein du collectif car il ne nécessite pas de grandes connaissances technique mais Bitwarden est plus simple pour les membres du collectif qui utiliseront les mots de passe.

## Où démarrer ?

**La solution la plus rapide pour démarrer en autonomie est Keepass**.

Si vous avez une instance Nextcloud de [partage de fichiers](partager-des-fichiers-et-des-calendriers.md), l'outil [Nextcloud Keeweb](https://apps.nextcloud.com/apps/keeweb) permet de gérer son premier coffre fort Keepass. C'est une extension à ajouter à votre instance Nextcloud. Autrement vous pouvez utiliser :

- [KeepassXC](https://keepassxc.org/), [MacPass](https://macpassapp.org/), [Keepass2](https://keepass.info/) : ce sont des logiciels à installer sur votre ordinateur pour créér le coffre fort. Vous partagez ensuite ce coffre fort selon vos propres modalités.
- [Keeweb.info](https://keeweb.info/) : vous pouvez consulter et gérer en ligne un coffre fort partagé entre plusieurs personnes via une solution en ligne de stockage de fichiers (cloud comme Nextcloud, Google Drive, Dropbox, ...).

**Si vous souhaitez privilégier Bitwarden ou Vaultwarden**, le collectif des chatons propose [plusieurs instances Vaultwarden](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=288&field_is_shared_value=All&title=). 