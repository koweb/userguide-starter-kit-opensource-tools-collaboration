---
title: "Partager des fichiers & des calendriers"
parent: Les besoins essentiels d'une équipe
nav_order: 4
---

# Partager des fichiers & des calendriers

Le partage de fichiers entre personnes passe par une solution où tous les documents sont centralisés sur un serveur central. Chaque membre de l’équipe accède aux fichiers d’où il veut et quand il veut pour travailler dessus.

![Partage de fichiers et des calendriers](../../assets/uploads/partage-de-fichiers-calendriers.jpg)

Les logiciels libres que nous avons sélectionné sont Nextcloud, OnlyOffice et Collabora Online.

## Nextcloud

Nextcloud est un espace de travail collaboratif en ligne avec un espace en ligne de stockage de fichiers pour y accéder de n'importe où avec une simple connexion et navigateur internet. Il offre aussi la possibilité de gérer ses agendas personnels et d’équipe.

## L'édition en ligne de documents

Deux suites bureautiques sont proposées avec Nextcloud : OnlyOffice et Collabora Online. OnlyOffice et Collabora Online sont des alternatives à OneDrive, Dropbox, Google Drive, Office365 et Google Docs.

Ces suites bureautiques rendent possible le traitement de texte, le tableur et le diaporama depuis un simple navigateur internet. Elles s’intègrent parfaitement à Nextcloud pour travailler à plusieurs en temps réel sur un même document et sans quitter son navigateur internet.

### Limitations

- L'édition en ligne sur mobile n'est en général possible qu'avec Collabora. Cependant peu pratique dans la réalité.
- Si vous êtes habitué-es à discuter via des commentaires comme sur Google Docs, vous devez utiliser en parallèle [une messagerie d'équipe](discuter-en-equipe.md) pour vous notifier les contributions.

### Découvrir ces solutions

- [OnlyOffice](https://www.onlyoffice.com/fr/) : l'éditeur parfait pour s'assurer une bonne compatibilité avec Word dans une interface similaire.
- [Collabora Online](https://nextcloud.com/fr/office/) : la solution privilégiée par les concepteurs de Nextcloud pour une intégration encore plus aboutie. L'interface est proche de celle de LibreOffice.

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [IndieHosters - Liiibre](https://indiehosters.net/) | Nextcloud/OnlyOffice | [15 jours](https://indiehosters.net/tester-liiibre/) | 8€ HT / mois / utilisateur actif |
| [Zaclys](https://www.zaclys.com/cloud-pro/) | Nextcloud/OnlyOffice | - | De 260€HT à 990€HT - Possibilité de création de compte personnel gratuit avec 2Go d'espace de stockage. |
| [Libretic](https://libretic.fr) | Nextcloud/Collabora Online | - | [À partir de 105€/an - adhésion comprise](https://libretic.fr/adherer) pour équipe de 10 personnes |
| [Framaspace](https://www.frama.space/abc/fr/) | Nextcloud/Collabora Online | - | Gratuit et réservé exclusivement aux associations et collectifs militants jusqu’à 50 membres |
| [OnlyOffice](https://www.onlyoffice.com/fr/) | OnlyOffice | 30 jours | [Gratuit pour organisme à but non lucratif](https://www.onlyoffice.com/fr/nonprofit-organizations.aspx) ou pour usage personnel. [A partir de 2,4€ HT / utilisateur / mois](https://www.onlyoffice.com/fr/for-small-business.aspx) pour les autres organisations |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

- [Nextcloud OnlyOffice](https://www.chatons.org/search/by-service?service_type_target_id=337&field_alternatives_aux_services_target_id=All&field_software_target_id=365&field_is_shared_value=All&title=)
- [Nextcloud Collabora](https://www.chatons.org/search/by-service?service_type_target_id=337&field_alternatives_aux_services_target_id=All&field_software_target_id=364&field_is_shared_value=All&title=)
- [Nextcloud](https://www.chatons.org/search/by-service?service_type_target_id=146&field_alternatives_aux_services_target_id=All&field_software_target_id=271&field_is_shared_value=All&title=)