---
title: "Prendre des décisions à distance"
parent: Les besoins essentiels d'une équipe
nav_order: 5
---

# Prendre des décisions à distance

Faire de la gouvernance partagée à distance présente des difficultés techniques. Le mouvement Occupy l’a bien compris en créant Loomio en 2013 pour prendre des décisions éclairées et égalitaires en ligne. Il s’agit d’un forum pourvu d’outils dédiés pour couvrir tous les cas d’arbitrage classiques, transposés au numérique.

![Prendre des décisions à distance](../../assets/uploads/prendre-des-decisions-a-distance.jpg)

## Les outils libres pour prendre des décisions à distance

- [Loomio](https://www.loomio.com/) est un logiciel de forum orienté vers les prises de décision. Vous discutez autour d'un sujet en ligne, par courriel ou mobile puis  grâce à des outils de sondage intégrés, la décision est à portée de mains.
- D'autres outils peuvent aider à prendre des décisions : les outils de sondage sont des moyens de prendre des décisions. Vous pouvez donc utiliser [Framadate](https://framadate.org/abc/fr/), ou l'extension de sondage [Poll](https://apps.nextcloud.com/apps/polls) proposée dans Nextcloud ou [Rocket.Chat](https://docs.rocket.chat/extend-rocket.chat-capabilities/rocket.chat-marketplace/rocket.chat-public-apps-guides/poll). Cependant pensez à associer au sondage un espace de discussions permettant au préalable des échanges.

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [IndieHosters - Liiibre](https://indiehosters.net/) | Rocket.Chat et Nextcloud Poll | [15 jours](https://indiehosters.net/tester-liiibre/) | 8€ HT / mois / utilisateur actif |
| Framasoft | [Framadate](https://framadate.org), [Loomio](https://framavox.org/abc/fr/)  | - | En libre accès |
| Loomio.org | [Loomio](https://loomio.org)  | 30 jours | A partir de 10$ / mois |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

- [Loomio](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=307&field_is_shared_value=All&title=)
- [Rocket.Chat](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=252&field_is_shared_value=All&title=)
- [Nextcloud](https://www.chatons.org/search/by-service?service_type_target_id=146&field_alternatives_aux_services_target_id=All&field_software_target_id=271&field_is_shared_value=All&title=)
- [Framadate](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=228&field_is_shared_value=All&title=)