---
title: "Trouver toutes les infos du collectif"
nav_order: 1
parent: Les besoins essentiels d'une équipe
---

# Trouver toutes les infos à jour et nécessaires au bon fonctionnement du collectif

Pour vous familiariser avec le concept de gare centrale, vous pouvez visionner cette vidéo

<iframe title="Le concept de gare centrale : des informations nécessaires au bon fonctionnement du collectif" src="https://tube.koweb.fr/videos/embed/30179a8f-7f5c-4f58-8eb4-2a57bff7ee1c" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

La gare centrale évolue avec la maturité du collectif. Elle peut-être au début un simple document en ligne qui liste les informations et les ressources essentielles du collectif.
Puis progressivement elle évolue pour prendre la forme d'un portail internet interne au collectif. Ce portail peut-être publique si le collectif a une politique d'ouverture, de partage et d'accueil.

![Discuter en équipe](../../assets/uploads/la-gare-centrale-du-collectif.jpg)

## Les outils libres pour une gare centrale

* [Etherpad](https://etherpad.org/) est un service de documents texte en ligne que l'on peut éditer en temps réel. Souple et rapide, il permet une mise en forme minimale et le suivi des modifications de chaque personne. Alternative à [Google Docs](https://docs.google.com/).
* [Wekan](https://wekan.github.io/) est un outil d'organisation d'informations sous la forme de kanban (post-its dans des colonnes pour rester simple 🙂). Ce type de d'outil offre la capacité d'associer des personnes aux informations présentées : ceci favorise la création de liens entre les personnes et permet de poser des questions. C'est donc pratique pour accueillir et offrir une assistance. Alternative à [Trello](https://trello.com).
* [YesWiki](https://yeswiki.net/?AccueiL) est l'outil libre de référence en France pour faciliter la coopération ouverte. A l'image d'une page blanche, ses usages sont quasiment illimités : ils dépendront de votre créativité ! Alternative à [Notion](https://www.notion.so/).

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [YesWiki Pro](https://yeswiki.pro/?PagePrincipale) | YesWiki | - | 66€ HT / an |
| [Colibris Outils Libres](https://www.colibris-outilslibres.org/) | [YesWiki](https://www.colibris-outilslibres.org/services/les-wikis-sites-collaboratifs-yeswiki/), [Etherpad](https://www.colibris-outilslibres.org/services/les-pads-ecrire-collectivement-etherpad/) | - | En libre accès |
| [Ethibox](https://ethibox.fr/) | [Wekan](https://ethibox.fr/wekan), [Etherpad](https://ethibox.fr/etherpad) | 7 jours | 19€ HT / mois avec 50G d'espace de stockage |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

* [Etherpad](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=224&field_is_shared_value=All&title=)
- [Wekan](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=290&field_is_shared_value=All&title=)
- [YesWiki](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=294&field_is_shared_value=All&title=)