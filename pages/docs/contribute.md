---
parent: A propos de ce guide et de Koweb
nav_order: 1
title: Proposer des modifications
---

# Contribuer en proposant des modifications

Pour contribuer à ce guide, vous devez

1. Créer un compte personnel sur [Gitlab.com](https://gitlab.com) et se connecter
2. Configurer votre compte pour une utilisation de Gitlab en français si ce n'est pas déjà le cas.

   * [Modifier votre fuseau horaire (timezone)](https://gitlab.com/-/profile)
   * [Modifier la langue par défaut](https://gitlab.com/-/profile/preferences) dans la section "Localisation"
3. Demander à rejoindre le groupe des rédacteurs. [Cliquez-ici pour afficher le groupe](https://gitlab.com/koweb/redacteurs) puis ensuite cliquez sur le lien `Demander l'accès` (où `Request Access` si vous êtes en anglais)
   ![Exemple écran pour demander l'accès](../../assets/uploads/redacteurs-group-koweb.jpg)
4. Se connecter sur [l'éditeur de la documentation](../../admin/index.html) en utilisant votre compte GitLab *une fois votre demande acceptée (vous recevrez une notification par courriels).*

**Besoin d'aide ?** : rejoignez la discussion concernant le guide sur [le forum Koweb Kafé](https://kafe.koweb.fr/t/guide-de-demarrage-pour-soutenir-le-travail-d-equipe-avec-les-outils-libres/771).

Le contenu modifié suit un processus de validation avant mise en ligne :

1. Votre modification est enregistrée par défaut dans un brouillon
2. Lorsque vous êtes satisfait-es, vous demandez sa publication en indiquant les contenus modifiés comme étant prêt
3. Une autre rédacteur-trice du guide [relie et publie](admin.html). Vous recevrez une notification envoyée par Gitlab contenant *"La demande de fusion ... a été fusionnée" (merge request en anglais).*

Toutes les contributions sont ainsi transparentes et chaque auteur est visible :

* [Historique des modifications fournie par GitLab](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/commits/main?ref_type=heads).
* [Statistiques sur les contributeurs-trices](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/graphs/main?ref_type=heads).

## Discuter de modifications

Une discussion est ouverte sur le forum Koweb Kafé pour échanger au sujet du guide : [rejoindre la discussion](https://kafe.koweb.fr/t/guide-de-demarrage-pour-soutenir-le-travail-d-equipe-avec-les-outils-libres/771)

## La modification du contenu

Une fois connecté sur l'éditeur de contenu avec votre compte Gitlab, vous pouvez naviguer dans les différentes collections de contenu :

* Paramètres et accueil : pour modifier différents paramètres comme le titre du guide, sa description et la page d'accueil
* Chapitres : les différents chapitres du guide. Ce sont les entrées proposées dans le menu.
  ![les chapitres dans le menu](../../assets/uploads/les-chapitres.jpg)
* Fiches de documentation : différentes fiches rangées dans un chapitre. Un chapitre peut ne pas avoir de fiches.

Voici comment donc va se présenter l'écran :

![accueil édition du contenu DecapCMS](../../assets/uploads/accueil-edition-contenu.jpg)

Il ne vous reste plus qu'à cliquer sur la collection de votre choix puis sur le contenu à modifier :

![exemple d'accès à l'édition d'un contenu](../../assets/uploads/chemin%20pour%20modifier%20un%20contenu.gif)

**En enregistrant, votre contenu est sauvegardé avec le statut de brouillon**.

![brouillon](../../assets/uploads/modification-enregistree-en-brouillon.jpg)

## La publication de vos modifications

Pour publier votre modification, vous devez passer le contenu modifié en statut "En cours de révision". Vous pouvez faire cela :

* Soit depuis l'écran d'édition du contenu
* Soit depuis le menu "Flux"

Un-e autre rédacteur-trice du guide le reliera puis le publiera. [La procédure est décrite ici](admin.html) et en tant que rédacteur-trice, vous pouvez aussi participer à la relecture et publication.

## Questions fréquentes

### Comment consulter ce qui a été modifié sur un contenu et discuter de ces modifications ?

Les modifications apportées par une personne sont visibles dans une **demande de fusion** sur Gitlab. [Consulter les demandes de fusion ici](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/merge_requests).
En ouvrant la demande de fusion, un onglet "Modifications" permet de visualiser les modifications sur chaque fichier source du contenu.

Sur chaque ligne de contenu, vous pouvez laisser une commentaire afin d'ouvrir une discussion. Gitlab vous enverra des notifications à chaque nouveau commentaire. Voici une démonstration :

![la demande de fusion](../../assets/uploads/la%20demande%20de%20fusion.gif)

### Comment lister les fiches par chapitre ?

Dans la collection "Fiches de documentation", choisir "Chapitres" dans le menu "Grouper par".

### Comment ordonner les contenus par numéro de chapitres ou pages ?

Dans la collection, choisir "Trier par" puis "Numéros de page" ou "Numéros de chapitre".