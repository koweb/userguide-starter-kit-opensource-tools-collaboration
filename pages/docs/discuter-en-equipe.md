---
title: "Discuter en équipe"
nav_order: 2
parent: Les besoins essentiels d'une équipe
---

# Discuter en équipe

Les solutions de “chat ” foisonnent, mais sont-elles toutes adaptées au travail en équipe ? Mattermost et Zulip sont des logiciels libres de messagerie instantanée « orienté équipes ». Au lieu de simplement créer des discussions entre des personnes, ces logiciels fournissent un espace dédié à une organisation ou des équipes pour organiser les échanges. Pour les équipes fonctionnnant au sein de réseaux, il ne faut pas négliger **Matrix** qui propose de fédérer les espaces de discussion.

![Discuter en équipe](../../assets/uploads/discuter-en-equipe.jpg)

## Les outils libres pour discuter en équipe

- [Mattermost](https://mattermost.com/) est une messagerie instantanée pour organiser les discussions en équipe. Elle regroupe de manière organisée toutes les discussions en un seul endroit. Alternative à [Slack](https://slack.com/intl/fr-fr/).
- [Element](https://element.io/) est un logiciel de discussions parmi d'autres pour discuter sur **un réseau utilisant le protocole Matrix**. Cette architecture permet à différentes équipes de s'interconnecter sans changer de logiciels.

Il existe d'autres outils comme Zulip par exemple. Pour choisir l'outil adapté en fonction de ses particularités, vous pouvez participer à la discussion [Comment faire le choix entre Mattermost, Rocket.Chat ou Zulip?](https://kafe.koweb.fr/t/comment-faire-le-choix-entre-mattermost-rocket-chat-ou-zulip/254)

## Où démarrer ?

| Hébergeur | Outils proposés | Période d'essai | Tarif |
|-----------|-----------------|-----------------|-------|
| [IndieHosters - Liiibre](https://indiehosters.net/) | Element/Matrix | [15 jours](https://indiehosters.net/tester-liiibre/) | 8€ HT / mois / utilisateur actif |
| Framasoft | [Mattermost](hhttps://framateam.org)  | - | En libre accès |
| [Colibris Outils Libres](https://www.colibris-outilslibres.org/) | [Mattermost](https://www.colibris-outilslibres.org/services/tchat-mattermost/) | - | En libre accès |
| [Ethibox](https://ethibox.fr/) | [Mattermost](https://ethibox.fr/mattermost) | 7 jours | 19€ HT / mois avec 50G d'espace de stockage |

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

- [Mattermost](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=250&field_is_shared_value=All&title=)
- [Matrix](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=429&field_software_target_id=274&field_is_shared_value=All&title=)
- [Zulip](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=612&field_is_shared_value=All&title=)

## Et Rocket.Chat ?

Dans sa version 6, l'entreprise derrière Rocket Chat a fait un choix stratégique : verrouiller des fonctionnalités essentielles pour les organisations. Il est possible de les débloquer uniquement avec l'achat d'une licence. Ainsi, le logiciel n'est plus vraiment un logiciel libre car ils adoptent un modèle **opencore**. Pour cette raison, nous avons retiré cet outil du guide. L'article très bien fait d'IndieHosters vous explique cela très bien : [Migrer de Rocket.Chat à Element](https://indiehosters.net/blog/2024/02/12/migrer-vers-matrix.html)