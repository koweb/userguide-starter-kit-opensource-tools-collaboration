---
parent: A propos de ce guide et de Koweb
nav_order: 2
title: Administrer ce guide
---

# Administrer ce guide

Le guide est maintenu par des personnes devant avoir au moins le rôle de `Maintainer` sur ce projet Gitlab ([voir les membres du projet](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/project_members)). C'est le cas de toute personne faisant partie des rédacteurs ([lire la raison dans les limites de gitlab/decapCMS](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/tree/main#les-limites-rencontr%C3%A9es))

## Comment traiter et publier les modifications faites par les rédacteurs-trices ?

Lorsque des rédacteurs-trices ont fait des modifications, des personnes doivent valider et mettre en ligne. Vous pouvez faire votre demande de relecture sur [la discussion associée au guide dans le forum](https://kafe.koweb.fr/t/guide-de-demarrage-pour-soutenir-le-travail-d-equipe-avec-les-outils-libres/771). Normalement [les autres rédacteurs-trices](https://gitlab.com/groups/koweb/redacteurs/-/group_members) reçoivent aussi des notifications de Gitlab informant que des demandes de fusion (merge request) sont faites.

Pour valider il faut :

1. [Consulter les demandes de fusion](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/merge_requests) et analyser les modifications (onglet *Modifications* de la demande de fusion).
2. Accepter la demande de fusion. Pour cela il y a deux façons de procéder :

   * Soit en cliquant sur le bouton bleu *Fusionner* et tous les contenus seront intégrés au guide.
   * Soit en se connectant à [l'administration du contenu](https://kit-outils-libres.koweb.fr/admin/) pour retravailler le contenu et ne publier que certaines modifications. Pour publier, il faut changer l'état du contenu en "*Prêt*" puis publier.
3. Générer le guide en ligne. Pour cela [se rendre sur une nouvelle exécution de pipeline](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/pipelines/new) puis faire *Exécuter le pipeline*.

## Démonstration pour basculer un contenu comme prêt puis le publier depuis l'éditeur

![workflow](/assets/uploads/workflow-decapcms.gif)

**Rappel** : la mise en ligne du guide n'est pas automatique une fois la publication faite. Il faut *exécuter le pipeline*. Pour cela [se rendre sur une nouvelle exécution de pipeline](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/pipelines/new) puis faire *Exécuter le pipeline*.