---
parent: Les besoins essentiels d'une équipe
nav_order: 3
title: Organiser des réunions
---
# Organiser des réunions

L’organisation de réunions à distance peut mettre en œuvre de nombreux logiciels libres pour trouver une date commune, réaliser une visioconférence et prendre des notes pendant la réunion.

![Organiser des réunions](../../assets/uploads/organiser-des-reunions.jpg "Slide organiser des réunions")

## Les outils libres pour organiser des réunions

* [Framadate](https://framadate.org) est un service en ligne de sondage édité par Framasoft. On peut y créer des sondages pour trouver une date commune de réunion. Alternative à [Doodle](https://doodle.com/fr/).
* [Jitsi](https://jitsi.org/) est une solution de vidéo-conférence accessible sur ordinateur, depuis votre navigateur web sans installation, ou par smartphone grâce à l’application Jitsi Meet. Alternative à [Google Meet](https://meet.google.com/).
* [Etherpad](https://etherpad.org/) est un service de prise de note collaborative en temps réel. Souple et rapide, il permet une mise en forme minimale et le suivi des modifications de chaque personne. Alternative à [Google Docs](https://docs.google.com/).
* [Scrumblr](https://framalibre.org/content/scrumblr) est un tableau virtuel sur lequel on peut coller des post-its. Cela permet des animations de réunion comme en vrai ! Alternative à de nombreux [outils de tableaux blancs](http://www.pearltrees.com/t/veille-collaborative-de-koweb/tableaux-blancs/id27334960).

## Où démarrer ?

| Hébergeur                                                        | Outils proposés                                                                                                                                                                                                                                                                                                                                                   | Période d'essai | Tarif          |
| ---------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------- | -------------- |
| [IndieHosters - Liiibre](https://indiehosters.net/)              | [Jitsi](https://meet.liiib.re)                                                                                                                                                                                                                                                                                                                                    | \-              | En libre accès |
| Framasoft                                                        | [Jitsi](https://framatalk.org/abc/fr/), [Framadate](https://framadate.org), [Etherpad](https://framapad.org/abc/fr/)                                                                                                                                                                                                                                              | \-              | En libre accès |
| [Colibris Outils Libres](https://www.colibris-outilslibres.org/) | [Jitsi](https://www.colibris-outilslibres.org/services/visioconference-jitsi-meet/), [Framadate](https://www.colibris-outilslibres.org/services/sondage-et-choisir-une-date-framadate/), [Etherpad](https://www.colibris-outilslibres.org/services/les-pads-ecrire-collectivement-etherpad/), [Scrumblr](https://www.colibris-outilslibres.org/services/post-it/) | \-              | En libre accès |
| [Scrumblr.ca](http://scrumblr.ca/)                               | Scrumblr                                                                                                                                                                                                                                                                                                                                                          | \-              | En libre accès |

> Les hébergeurs Jitsi (visioconférence) en libre accès peuvent fournir un service dégradé en fonction de leur popularité. Prévoyez une solution de secours avec par exemple [kMeet](https://www.infomaniak.com/fr/kmeet)

Plus d'hébergeurs proposés sur [chatons.org](https://www.chatons.org/) :

* [Jitsi](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=225&field_is_shared_value=All&title=)
* [Etherpad](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=224&field_is_shared_value=All&title=)
* [Framadate](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=228&field_is_shared_value=All&title=)
* [Scrumblr](https://www.chatons.org/search/by-service?service_type_target_id=All&field_alternatives_aux_services_target_id=All&field_software_target_id=230&field_is_shared_value=All&title=)