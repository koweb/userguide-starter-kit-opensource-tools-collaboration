---
title: Accueil
---
# Soutenir le travail d'équipe avec les outils libres

**Ceci est un guide de démarrage** pour découvrir les principaux besoins et outils nécessaires pour qu'une équipe collabore en s'appuyant sur des outils numériques libres.

![Guide de démarrage](assets/uploads/kit-demarrage-avril-2021.jpg)

Ce guide évolue au fur et à mesure des contributions et des nouveautés : nouveaux besoins d'équipe, nouveaux outils, nouveaux hébergeurs.

* Pour être informé des futures versions, [vous pouvez vous abonner ici](https://www.koweb.fr/kit-outils-libres#form).
* Pour participer à ses mises à jours : [lire la section pour contribuer](pages/docs/contribute.html)

## Que sont des outils libres ?

Chaque logiciel fonctionne sur la base **d’un code source**. Les logiciels libres se différencient des logiciels propriétaires car **leur code source est ouvert** :

* Tout le monde peut consulter le code source

Ensuite des **droits sur le code source sont donnés en échange de devoirs**. C’est une licence relevant du domaine de la propriété intellectuelle qui définit cela. La licence la plus ouverte est une licence **respectant les 4 libertés fondamentales du libre ET permettant de les protéger**. On la dit généralement compatible **copyleft**.

**Les libertés fondamentales**

* **la liberté de faire fonctionner le programme** comme vous voulez, pour n'importe quel usage
* **la liberté d'étudier le fonctionnement du programme, et de le modifier** pour qu'il effectue vos tâches informatiques comme vous le souhaitez
* **la liberté de redistribuer des copies**
* **la liberté de distribuer** aux autres des copies de **vos versions modifiées** 

> Pour comprendre avec des choses plus concrètes, on peut comparer cela à une voiture pour laquelle nous aurions les plans avec le droit de créer une nouvelle voiture légèrement différente car celle proposée avait 4 places et vous en avez besoin de 5.

## Des fiches pratiques pour vous guider sur chaque besoin fondamental de collaboration

Chaque fiche pratique vous propose :

* Une présentation du besoin couvert par la fiche
* Les principaux outils libres couvrant ce besoin
* Les outils propriétaires que ces outils libres remplacent
* Où démarrer : une sélection d'organisation proposant cet outil et à quel prix

## La licence de ce guide

![licence creative commons CC-BY-SA](assets/uploads/licence.jpg)

Lire : [Licence du guide](pages/docs/licence.html)

## Quels sont les outils libres utilisés pour créer ce guide ?

Vous pouvez consulter le fichier [Readme](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/blob/main/README.md) (Gitlab.com) du dépôt contenant tout le guide.