# Contribuer

- Pour contribuer au contenu : [lire cette section du guide : contribuer](https://kit-outils-libres.koweb.fr/pages/docs/contribute.html)
- Pour contribuer au code source de génération du guide, rendez-vous dans le forum Koweb : [le Koweb Kafé](https://kafe.koweb.fr)
