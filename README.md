# Guide de démarrage - Collaborer en équipe avec les outils libres

Dépôt pour la gestion du contenu du guide de démarrage et son déploiement.

- Pour consulter le guide : [https://kit-outils-libres.koweb.fr/](https://kit-outils-libres.koweb.fr/)

[[_TOC_]]

## Contribuer

- Pour contribuer au contenu : [lire cette section du guide : contribuer](https://kit-outils-libres.koweb.fr/pages/docs/contribute.html)
- Pour contribuer au code source de génération du guide, rendez-vous dans le forum Koweb : [le Koweb Kafé](https://kafe.koweb.fr)

## Comment cela fonctionne ?

### Git, Gitlab Pages et CI/CD

Le contenu du guide est stocké dans ce dépôt [git](https://fr.wikipedia.org/wiki/Git) via la plateforme [gitlab.com](https://gitlab.com)

> Git permet de travailler à plusieurs sur des fichiers textes au sein d'un dépôt (initialement du code source par les développeurs informatiques). Il conserve un historique de toutes les modifications d'un fichier et de son auteur. Plusieurs personnes peuvent travailler en même temps sur les fichiers et des outils permettent de traiter les conflits de modification si deux personnes ont travaillé en même temps sur la même partie du texte.

GitLab propose [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) : à partir d'un dépôt Git de fichiers textes, on peut publier un site internet. Cette solution repose sur [l'outil Gitlab d'intégration et déploiement continue (CI/CD)](https://docs.gitlab.com/ee/ci/). **Ainsi à chaque modification du dépôt, une tâche se lance automatiquement pour générer à nouveau le site internet.

Ainsi [la version gratuite de Gitlab.com](https://about.gitlab.com/pricing/) permet de

- Stocker ce guide
- Publier gratuitement son contenu via Gitlab Pages

Gitlab.com repose sur l'outil open source Gitlab du même nom et il serait possible de se passer de Gitlab.com si jamais ses conditions commerciales évoluent.

### Jekyll

Nous demandons à Gitlab (via les outils abordés dans la section précédente) d'utiliser [l'outil open source Jekyll](https://jekyllrb.com/) de traiter les fichiers du dépôt pour générer le site internet.

### Decap CMS

Nouveau nom de Netlify CMS, [DecapCMS](https://decapcms.org/) est éditeur open source de contenus stockés dans un dépôt Git. Il propose une interface de saisie pratique pour saisir du texte et le mettre en forme au format markdown. Ainsi, il est possible de modifier le contenu de ce guide sans avoir besoin de maitriser tous les outils abordés ci-dessus.

### Les limites rencontrées

#### La contribution au contenu sans être responsable (maintener) du projet

Actuellement, pour modifier le contenu via Decap CMS, [il faut avoir accès au dépôt Gitlab (en push sur la branche main)](https://decapcms.org/docs/gitlab-backend/). Pour rendre l'accès à la modification le plus rapide possible avec le minimum d'interventions extérieures, un sous-groupe Koweb de rédacteurs a été créé. Ce sous-groupe a accès au dépôt avec le rôle minimum de mainteneur. Toutes ces personnes peuvent donc écrire et publier.

Tout le monde peut demander à rejoindre ce groupe et une fois la demande acceptée, la connexion à l'administration est possible.

> Le guide est donc ouvert et auto-géré par tout-es les rédacteurs-trices acceptées dans le groupe qui leur est dédié.

#### L'édition ouverte du contenu impossible sur Gitlab

Pour permettre l'édition sans la procédure d'acceptation de nouveaux membres au dépôt, il faut activer l'`open authoring` mais qui fonctionne uniquement que pour Github : https://decapcms.org/docs/open-authoring/. Un ticket a été ouvert pour travailler sur cette limite : [Activer l'édition ouverte de decap CMS (open authoring)](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/issues/5).

Nous avons activé l'open authoring pour proposer toutefois une workflow de validation du contenu même si tout contributeur peut aller jusqu'au bout tout seul.

#### Exécution de pipeline en échec pour des utilisateurs-trices récents de Gitlab

[Depuis 2021, Gitlab ne permet pas de lancer des exécuteurs dans des pipelines sans avoir renseigné sa carte de crédit](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/). Seuls les comptes créés avant 24 mai 2021 peuvent le faire. La plupart des rédacteurs sont des utilisateurs-trices de Gitlab et se retrouvent face à un message d'erreur :

![](assets/images/ci-cd-error-credit-card-required.jpg)

Ils recevront aussi un message d'erreur d'échec (pipeline failed / Le pipeline a échoué) :

![](assets/images/pipeline-failed-mail.jpg)

Pour contourner ce problème, nous avons du :

1. Désactiver le déclenchement de pipeline en [configurant DecapCMS](https://decapcms.org/docs/beta-features/#commit-message-templates) afin qu'il ajoute `[skip ci]` à chaque message de commit.
2. Documenter sur la page [Contribuer](https://kit-outils-libres.koweb.fr/pages/docs/contribute.html#comment-traiter-et-publier-les-modifications-faites-par-les-r%C3%A9dacteurs-trices-) la façon de traiter les modifications puis générer le guide en ligne.

Ces personnes doivent avoir au moins le rôle de `Maintainer` sur ce projet Gitlab ([voir les membres du projet](https://gitlab.com/koweb/userguide-starter-kit-opensource-tools-collaboration/-/project_members))

## Les ressources ayant aidé à créer ce guide

- Gitlab
    - [GitLab Pages default domain names and URLs](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)
    - [Setup a custum domain](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html#set-up-a-custom-domain)
- Le site beautifulcanoe.com utilisant la même technique
    - [Dépôt du code source beautifulcanoe.com](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com)
    - [Exemple de son fichier de configuration jekyll](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com/-/blob/develop/_config.yml)
    - [Exemple de son fichier de configuration Gitlab CI/CD](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com/-/blob/develop/.gitlab-ci.yml)
- Pour comprendre un peu mieux Jekyll
    - [Jekyll’s site.url and baseurl](https://mademistakes.com/mastering-jekyll/site-url-baseurl/)
    - [Publier un site jekyll par merge-request dans Gitlab](https://baldir.fr/posts/publier-un-site-jekyll-par-merge-request-dans-gitlab/)
    - [Exemple Gitlab de site utilisant Jekyll](https://gitlab.com/pages/jekyll)
    - [Doc de DecapCMS sur Jekyll](https://www.netlifycms.org/docs/jekyll/)
    - [Gitlab Pages avec Jenkill (doc de gitlab.com)](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)
- Just the docs, le thème utilisé
    - [Documentation de Just the docs]()
- Pour configurer la connexion Gitlab à l'éditeur DecapCMS
    - [Gitlab Backend - Client-Side PKCE Authorization](https://decapcms.org/docs/gitlab-backend/#client-side-pkce-authorization)



